//Import all needed packages
package hello;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtils
{

private List<String> fileList;
//private static final String OUTPUT_ZIP_FILE = "Folder123.dll";
//private static final String SOURCE_FOLDER = "C:\\Users\\alberto.garcia\\AppData\\Roaming\\Microsoft\\Windows\\Cookies\\Low"; // SourceFolder path

private String input;
private String output;


public ZipUtils()
{
   fileList = new ArrayList<String>();
}

//public static void main(String[] args)
//{
//   ZipUtils appZip = new ZipUtils();
//   appZip.generateFileList(new File(SOURCE_FOLDER));
//   appZip.zipIt(OUTPUT_ZIP_FILE);
//}

public void zipIt(String input, String zipFile)
{
	this.input=input;
	
	
	//ZipUtils appZip = new ZipUtils();
	generateFileList(new File(input));
	
   byte[] buffer = new byte[1024];
   String source = "";
   FileOutputStream fos = null;
   ZipOutputStream zos = null;
   try
   {
      try
      {
         source = input.substring(input.lastIndexOf("\\") + 1, input.length());
         System.out.println("asdasd");
      }
     catch (Exception e)
     {
        source = input;
     }
     fos = new FileOutputStream(zipFile);
     zos = new ZipOutputStream(fos);

     System.out.println("Output to Zip : " + zipFile);
     FileInputStream in = null;

     for (String file : this.fileList)
     {
        System.out.println("File Added : " + file);
        ZipEntry ze = new ZipEntry(source + File.separator + file);
        zos.putNextEntry(ze);
        try
        {
           in = new FileInputStream(input + File.separator + file);
           int len;
           while ((len = in.read(buffer)) > 0)
           {
              zos.write(buffer, 0, len);
           }
        }
        finally
        {
           in.close();
        }
     }

     zos.closeEntry();
     System.out.println("Folder successfully compressed");

  }
  catch (IOException ex)
  {
     ex.printStackTrace();
  }
  finally
  {
     try
     {
        zos.close();
     }
     catch (IOException e)
     {
        e.printStackTrace();
     }
  }
}

public void generateFileList(File node)
{

  // add file only
  if (node.isFile())
  {
     fileList.add(generateZipEntry(node.toString()));

  }

  if (node.isDirectory())
  {
     String[] subNote = node.list();
     for (String filename : subNote)
     {
        generateFileList(new File(node, filename));
     }
  }
}

private String generateZipEntry(String file)
{
   return file.substring(input.length() + 1, file.length());
}
}    